# SITE Faculty App -Service#
Project Code: SITE0003

SITE Faculty App- Service is a part of SITE Applications. 
## Use Cases ##

### Use Case Diagram ###
https://bitbucket.org/kkconsultancy/site-faculty/commits/0077101d50851f696a21d81ef0e36a54c1490d3c#chg-Facultyusecase.drawio.jpg

## User Stories ##
### SITE0003-01: LOGIN ###

* Actor: Faculty
* Pre Condition:

1. User should have Faculty ID registered in database.

* Input:

1. Faculty ID
2. Email ID
3. Password

* Sequence:

1. User opens Site Faculty App and enter credentials click on 'SUBMIT'.
2. Redirects to Home page appears when valid Faculty ID,Email ID and Password.
3. It redirects to Home page displays buttons like View profile,Update,View,Suggestions,Share and Logout.

* Post Condition:

1. Page displays logggined successfully message and the home page appears.

* Exceptions:
1. Displays Faculty ID is invalid when given invalid FacultyID.
2. Displays Forgot password in login page  when the entered Password is incorrect.
3. click on "Forgot Password" page redirects to Reset password page.
4. User enters new password after confirmation password is complete.
5. Submit new password page redirects to Login intent to Login again.

### SITE0003-02: View Profile with SITEFaculty APP ID ###

* Actor: Faculty 

* Pre Condition:

1. User should have  Loggined Sucessfully with valid Faculty ID and password.

* Input:

1. Faculty ID.

* Sequence:

1. User clicks the ViewProfile button in home page. 
2. Profile page appears.
3. Page displays Faculty details along with "Change Password and Attendance" buttons.
4. User can click on "Change Password" to change his/her current Password. 

* Post Condition:

1. Page displays Faculty details.
2. Password changed successfully when click on "Change Password" button.
3. Page displays Faculty Attendance ,user click on View Attendace.

* Exceptions:

1. Login page re-appears with appropriate message if user enters Invalid Faculty ID.
2. Prompt box re-appears with appropriate message if user Faculty ID is not present in database.
3. Login page re-appears with appropriate message if user fails to enter valid Password.

### SITE0003-03: Update with SITE FacultyApp ID###

* Actor: Faculty

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Faculty ID and Password.


* Input:

1. Facutly ID.
2. Password.

* Sequence:
 
1. User click on Update button.
2. User Update page appears.
3. User click selective buttons Update Marks,Update Attendance.
4. Displays student update marks include mid marks ,laboratory marks and class test marks.

* Post Condition:

1. User update Marks page appears with name of the student displayed.

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Faculty ID.
2. Update page re-apperars with appropriate message if faculty enters ivalis student ID.

### SITE0003-04 View with SITE FacultyApp ID###

* Actor: Faculty
 
* Pre Condition:

1. User should have  Loggined Sucessfully with valid Faculty ID and Password.

* Input:
1. Faculty ID.
2. Password.
3. Student ID.

* Sequence:

1. User opens Home page.
2. User clicks on View button.
3. User clicks on View Marks,View Attendance,View Profile.
4. User enters Student ID to view the Student details.
5. User View Student Marks,Attendace and Profile with name of the student displayed.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Faculty ID.
2. Prompt box re-appears with appropriate message if user enters invalid Student ID.


### SITE0003-05 Suggestions with SITE Faculty App ID ###

* Actor: Faculty

* Pre Condition:

1. User should have Loggined Sucessfully with valid Faculty ID and Password.

* Input:

1. Faculty ID.

* Sequence:

1. User click on Suggestions button.
2. Page displays Rating Status and Feedback. 
3. User enters/skips feedback.
4. User click on 'Submit' to Rating and Feedback
5. User click on 'cancel' to Redirect Home page.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

### SITE0003-06 Share with SITE FacultyApp ID ###

* Actor: Faculty

* Pre Condition:

1. User should have  Loggined Sucessfully with valid Facutly ID and password.

* Input:

1. Faculty ID.

* Sequence:

1. User click on Share button.
2. Page displays apps whatsapp,facebook other social media apps. 
3. User enters/skips share to promote.
4. User click on 'Submit' to Share.
5. User click on 'cancel' to Redirect Home page.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Faculty ID.


### SITE0003-07 Logout with SITE FacultyApp ID ###

* Actor: Facutly

* Pre Condition:

1. . User should have  Loggined Sucessfully with valid Facutly ID and password.

* Input:

1. Faculty ID.

* Sequence:

1. User click on Logout button.
2. Page displays prompt succesfully logged out.
3. Page displays login page after logout. 
3. User enters/skips Logout.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

None

## Workflow ##

### Workflow Diagram ###
https://bitbucket.org/kkconsultancy/site-faculty/commits/0077101d50851f696a21d81ef0e36a54c1490d3c?at=master#chg-Facultyappworkflow.drawio.jpg

## Development ##

### Dependencies ###
1. Android Studio. 
2. Git

### How to run ###
1.Open the Android Studio.
2. create a new project.
3.write the code on java.
4.Run the app using emulator or connected device.

2. Install the dependencies
1. Open chrome.
2. Install Android Studio with latest version.
3. Install git.
4. Install Java JDK to support Android Studio and set the path in pc.  
3. Run the app
1. Open mobile phone
2. Open playstore.
3. Install SiteFacultyApp
4. Run it by login.
4. Verify http://localhost:8081
